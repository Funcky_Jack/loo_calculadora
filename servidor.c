//ficheiro: servidor.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

void escrever(int socket_fd, const char * mensagem){
     /*Na linguagem C, uma string tem sempre o caracter finalizador '\0'
      * o tamanho da mensagem inclui todos os seus caracteres e o 
      * caracter finalizador 
      */
     int tamanho = strlen(mensagem) + 1;
     
     //escrever o tamanho de bytes da mensagem no socket
     write (socket_fd, &tamanho, sizeof(tamanho));
     //escrever mensagem no socket
     write(socket_fd, mensagem, tamanho);     
}

void calculadora(char* s, int socket){
     
     int v[2], i = 0, resultado=0;
     char aux[100] = "";

     if(!(strchr(s,'+') == NULL)){
          char *token = strtok(s,"+");
          while(token){
               char *a = token; 
               v[i++] = atoi(a);
               token = strtok(NULL, "+");
          }
          resultado = v[0] + v[1]; 
          sprintf(aux, "%d+%d=%d\n", v[0], v[1], resultado);
     }
     
     if(!(strchr(s,'-') == NULL)){
          char *token = strtok(s,"-");
          while(token){
               char *a = token; 
               v[i++] = atoi(a);
               token = strtok(NULL, "-");
          }
          resultado = v[0] - v[1];          
          sprintf(aux, "%d-%d=%d\n", v[0], v[1], resultado);
     }

     if(!(strchr(s,'*') == NULL)){
          char *token = strtok(s,"*");
          while(token){
               char *a = token; 
               v[i++] = atoi(a);
               token = strtok(NULL, "*");
          }
          resultado = v[0] * v[1];          
          sprintf(aux, "%d*%d=%d\n", v[0], v[1], resultado);                  
     }

     if(!(strchr(s,'/') == NULL)){
          char *token = strtok(s,"/");
          while(token){
               char *a = token; 
               v[i++] = atoi(a);
               token = strtok(NULL, "/");
          }
          if(v[1]==0){               
               sprintf(aux, "ERRO divisão por 0\n");               
          }
          else{
               resultado = v[0] / v[1];               
               sprintf(aux, "%d/%d=%d\n", v[0], v[1], resultado);               
          }         
     }    
     
     printf("%s", aux);
     escrever(socket, aux);
}

/*Função para ler do socket*/
int ler (int cliente_socket){
     
     int tamanho, resultado;

     while(1){
     	if(read(cliente_socket, &tamanho, sizeof(tamanho)) == 0)
        	return 0;//não tem nada no socket
          
        // Reservar espaco em memoria para guardar a mensagem
        char *mensagem = (char*) malloc(sizeof(mensagem));

        // Ler a mensagem do cliente
        read(cliente_socket, mensagem, tamanho);
                
        printf("servidor: a receber mensagem %s\n", mensagem);        
        
	    calculadora(mensagem, cliente_socket);
	        
	    free(mensagem);
	    
	    if(strcmp(mensagem, "end") == 0){
	          escrever(cliente_socket, mensagem);
	          free(mensagem);
	          close(cliente_socket);     		  
        }
     }
}

int main(int argc, char *argv[]){
     const char* const nome_socket = argv[1];

     //variaveis do socket
     int socket_fd; //descritor do socket          
     struct sockaddr_un nome_cliente;
     
     struct sockaddr_un nome;
     socklen_t cliente_nome_len;
     int cliente_socket_fd;
     
     pthread_t thread_cliente;     
     
     socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);
     
     nome.sun_family = AF_LOCAL;
     strcpy(nome.sun_path, nome_socket);
     
     bind(socket_fd, (struct sockaddr *) &nome, SUN_LEN(&nome));
     
     listen(socket_fd, 3);
     while(1){

          cliente_socket_fd = accept(socket_fd, (struct sockaddr *) &nome_cliente, &cliente_nome_len);
          
          ler(cliente_socket_fd);
          
          
	     
          
     }

   //inicia a thread de comunicação
	//pthread_create(&lista_threads[indice], NULL, &comunicacao ,client_socket_fd);
     //close(socket_fd);
     //unlink(nome_socket);

     return 0;
}
