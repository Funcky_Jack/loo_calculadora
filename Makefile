# Nome dos executáveis
all: servidor cliente

# O executável cliente depende do objeto clinte.o
cliente: cliente.o
	gcc -o cliente cliente.o

# O objecto cliente.o depende do ficheiro cliente.c
cliente.o: cliente.c
	gcc -c cliente.c

# O executável servidor depende do objeto servidor.o	
servidor: servidor.o
	gcc -o servidor servidor.o -lpthread

# O objecto servidor.o depende do ficheiro servidor.c	
servidor.o:
	gcc -c servidor.c

# Remove ficheiros	
clean:
	rm -f teste *.o servidor cliente
