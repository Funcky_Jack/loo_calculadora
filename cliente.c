//ficheiro: cliente.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

/*Função para escrever no socket*/
void escrever(int socket_fd, const char * mensagem){
     /*Na linguagem C, uma string tem sempre o caracter finalizador '\0'
      * o tamanho da mensagem inclui todos os seus caracteres e o 
      * caracter finalizador 
      */
     int tamanho = strlen(mensagem) + 1;
     
     //escrever o tamanho de bytes da mensagem no socket
     write (socket_fd, &tamanho, sizeof(tamanho));
     //escrever mensagem no socket
     write(socket_fd, mensagem, tamanho);
}

int ler_socket(int cliente_socket){
     
     int tamanho, sair=0;

     while(sair==0){
          if(read(cliente_socket, &tamanho, sizeof(tamanho)) == 0)
               return 0;//não tem nada no socket
          //reservar espaço para a mensagem
          char *mensagem = (char *) malloc(tamanho);
          read(cliente_socket, mensagem, tamanho);
          printf("%s",mensagem);
          sair=1;
     }
     return 0;
}

int main(int argc, char *argv[]){
	
     //primeiro argumento é o nome do servidor/socket
     const char * const nome_socket = argv[1];
     
     //estabelecer o nome para o servidor
     struct sockaddr_un nome;
     
     int socket_fd;
     char aux[100];
     
	do{     
		do{
        	  printf("Introduza start para começar\n");
        	  scanf("%s", aux);
              
    		}while(strcmp(aux,"start") != 0);
            
            	socket_fd = socket(PF_LOCAL, SOCK_STREAM, 0);
                
                nome.sun_family = AF_LOCAL;
     			strcpy(nome.sun_path, nome_socket);

    		 	connect(socket_fd, (struct sockaddr*) &nome, SUN_LEN(&nome));
     
				 do{
		  		  	printf("Introduza o cálculo no seguinte formato (númerooperaçãonúmero)\n");
		    		scanf("%s", aux);
		            
		    		escrever(socket_fd, aux);
		            
				 	ler_socket(socket_fd); 

		 		}while(strcmp(aux, "end") != 0);
		        
		        close(socket_fd);
 	}while(1);
     
//close(socket_fd);     
     return 0;
}
